FROM frolvlad/alpine-oraclejdk8:slim

ADD target/qservice-1.0.0.jar app.jar
RUN sh -c 'touch /app.jar'

ENTRYPOINT [ "sh", "-c", "java -jar -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=docker /app.jar" ]