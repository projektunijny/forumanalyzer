#Zadanie 2.5
##Badania nad opracowaniem nowych metod analiz dyskusji na forach eksperckich

Serwis w tym repozytorium pokazuje efekty prac nad zadaniem 2.5. Za jego pomocą można uzyskać dostęp do zgromadzonych danych serwisu _stackOverflow_.

##Metody uruchomienia

1. Standardowe uruchomienie SpringBoot

   * Przygotowanie bazy danych według wytycznych w pkt _baza danych_
   *     mvn clean package
   *     cd target
   *     java -jar qservice-1.0.0.jar
   
2. Skonstruowanie obrazu docker
   * Skopiowanie dumpa bazy (format .sql) do _docker/data_
   * skonstruowanie obrazu docker (np. wykorzystanie skryptu _docker/build.cmd_)
   *     docker-compose up
   
##Instrukcja obsługi

Aplikacja wystawiona jest na localhost:8110. Jest to standardowy REST + 2 widoki dla uproszczenia kodowania
Opis zapytań:

 * http://localhost:8110/expert/${id}/ -weryfikacja czy istnieje ekspert o podanym id (odpowiedź 200 lub 404)
 * http://localhost:8110/expert/${id}/${adres konkretnego zapytania} - zapytanie o wskaźniki oceny eksperta
    * /expert/{expertId}/questionPosted
    * /expert/{expertId}/answersGiven
    * /expert/{expertId}/acceptedAnswers
    * /expert/{expertId}/duplicatedQuestions
    * /expert/{expertId}/discussionsParticipated
    * /expert/{expertId}/commentsGiven
    * /expert/{expertId}/expertReached
    * /expert/{expertId}/profileCreation

Dokłady opis wskaźników znajduje się w raporcie z badań (confluence). Wyniki każdego zapytania można ograniczyć do konkretnej dziedziny (lub wielu) poprzez dodanie parametru _tag_. Parametrów można dodawać wiele, oddzieliwszy je przeciwnkami, np

    http://localhost:8110/expert/1/questionPosted?tag=java
lub

    http://localhost:8110/expert/1/questionPosted?tag=java,html

Zapytania szukają części wspólnych, więc w drugim przykładzie tylko takich pytań, które dotyczą zarówno tagu _java_ jak i _html_

##Baza danych
Baza danych powinna zawierać informacje pobrane ze stacka na temat postów użytkowników i komentarzy. Przykład poprawnej bazy danych o właściwej strukturze znajduje się w _docker/data_
Aplikacja jest skonfigurowana do korzystania z lokalnej bazy danych MySQL pod portem 3306 o nazwie stack
    
    url=jdbc:mysql://localhost:3306/stack
    login=pu
    password=puPassword

Przy ręcznym uruchamianiu (metoda 1) należy zapewnić, aby odpowiednia baza danych istniała, była dostępna i miała właściwą strukturę.
Przy wykorzystaniu metody 2 wystarczy zapewnić, żeby odpowiedni dump znajdował się w _docker/data_.

Zrzut pełnej bazy danych można pobrać z adresu:

   http://www.klecha.cc/stack.7z
   
Dla wydajnego działania należy zweryfikować czy baza zawiera następujące indeksy:

 * INDEX(post_id), tabela comment
 * INDEX(user_id), tabela comment
 * INDEX(parent_id), tabela post
 * INDEX(owner_user_id), tabela post
 * INDEX(accepted_answer_id), tabela post
 * INDEX(post_id), tabela post_link
    