package pl.michalklecha.soexperts.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.michalklecha.soexperts.model.domain.Post;
import pl.michalklecha.soexperts.repository.PostRepository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class PostCacheImpl implements PostCache {
    private static final int MAX_SIZE = 10;
    private PostRepository postRepository;

    private HashMap<Integer, List<Post>> posts = new HashMap<>();
    private LinkedList<Integer> order = new LinkedList<>();

    @Autowired
    public PostCacheImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> getPosts(int expertId) {
        prepareUser(expertId);

        checkSize();
        return posts.get(expertId);
    }

    @Override
    public void prepareUser(int expertId) {
        if (!posts.containsKey(expertId)) {
            preparePosts(expertId);
        }
    }


    private void preparePosts(int expertId) {
        posts.put(expertId, postRepository.findAllByOwnerUserId(expertId));
        order.add(expertId);
    }

    private void checkSize() {
        if (posts.size() >= MAX_SIZE) {
            posts.remove(order.getFirst());
            order.removeFirst();
        }
    }
}
