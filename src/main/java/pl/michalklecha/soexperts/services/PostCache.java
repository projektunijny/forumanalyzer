package pl.michalklecha.soexperts.services;

import pl.michalklecha.soexperts.model.domain.Post;

import java.util.List;

public interface PostCache {
    List<Post> getPosts(int expertId);

    void prepareUser(int expertId);
}
