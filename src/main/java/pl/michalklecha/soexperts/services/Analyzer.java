package pl.michalklecha.soexperts.services;

import java.util.Date;
import java.util.List;

public interface Analyzer {

    void prepareUser(Integer expertId);

    void checkIfUserExists(Integer expertId);

    int getNumberOfQuestions(Integer expertId, List<String> tags);

    int getNumberOfAnswers(Integer expertId, List<String> tags);

    int getNumberOfAcceptedAnswers(Integer expertId, List<String> tags);

    int getNumberOfDuplicatedQuestions(Integer expertId, List<String> tags);

    int getNumberOfDiscussionsParticipated(Integer expertId, List<String> tags);

    int getNumberOfComments(Integer expertId, List<String> tags);

    Date getProfileCreationDate(Integer expertId);

    int getNumberOfExpertReached(Integer expertId, List<String> tags);
}
