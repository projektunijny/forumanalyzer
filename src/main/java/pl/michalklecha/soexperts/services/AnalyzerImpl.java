package pl.michalklecha.soexperts.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.michalklecha.soexperts.exceptions.ResourceNotFoundException;
import pl.michalklecha.soexperts.model.domain.Post;
import pl.michalklecha.soexperts.model.domain.SeUser;
import pl.michalklecha.soexperts.model.domain.types.LinkType;
import pl.michalklecha.soexperts.model.domain.types.PostType;
import pl.michalklecha.soexperts.repository.CommentRepository;
import pl.michalklecha.soexperts.repository.PostLinkRepository;
import pl.michalklecha.soexperts.repository.PostRepository;
import pl.michalklecha.soexperts.repository.UserRepository;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class AnalyzerImpl implements Analyzer {

    private PostCache usersPosts;

    private UserRepository userRepository;
    private PostRepository postRepository;
    private CommentRepository commentRepository;
    private PostLinkRepository postLinkRepository;

    @Autowired
    public AnalyzerImpl(PostCache usersPosts, UserRepository userRepository, PostRepository postRepository, CommentRepository commentRepository, PostLinkRepository postLinkRepository) {
        this.usersPosts = usersPosts;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.postLinkRepository = postLinkRepository;
    }

    //region prepare
    @Override
    public void prepareUser(Integer expertId) {
        usersPosts.prepareUser(expertId);
    }

    @Override
    public void checkIfUserExists(Integer expertId) {
        SeUser seUser = userRepository.findOne(expertId);
        if (seUser == null) {
            throw new ResourceNotFoundException();
        }
    }
    //endregion

    @Override
    public int getNumberOfQuestions(Integer expertId, List<String> tags) {
        Stream<Post> stream = usersPosts.getPosts(expertId).stream()
                .filter(post -> post.getPostTypeId() == PostType.QUESTION);

        if (tags.size() > 0) {
            stream = stream.filter(post -> post.containsTags(tags));
        }
        return (int) stream.count();
    }

    @Override
    public int getNumberOfAnswers(Integer expertId, List<String> tags) {
        Stream<Post> stream = usersPosts.getPosts(expertId).stream()
                .filter(post -> post.getPostTypeId() == PostType.ANSWER);

        if (tags.size() > 0) {
            Set<Integer> ids = stream.map(Post::getParentId).collect(Collectors.toSet());
            List<Post> posts = postRepository.findAllByIdIn(ids);
            return (int) posts.stream().filter(post -> post.containsTags(tags)).count();
        }

        return (int) stream.count();
    }

    @Override
    public int getNumberOfAcceptedAnswers(Integer expertId, List<String> tags) {
        List<Integer> usersAnswersIds = usersPosts.getPosts(expertId).stream()
                .filter(p -> p.getPostTypeId() == PostType.ANSWER)
                .map(Post::getId)
                .collect(Collectors.toList());
        if (tags.size() > 0) {
            List<Post> posts = postRepository.findAllByAcceptedAnswerIdIn(usersAnswersIds);
            return (int) posts.stream().filter(post -> post.containsTags(tags)).count();
        }
        return postRepository.countByAcceptedAnswerIdIn(usersAnswersIds);
    }

    @Override
    public int getNumberOfDuplicatedQuestions(Integer expertId, List<String> tags) {
        Stream<Post> questions = usersPosts.getPosts(expertId).stream()
                .filter(p -> p.getPostTypeId() == PostType.QUESTION);

        if (tags.size() > 0) {
            questions = questions.filter(post -> post.containsTags(tags));
        }

        List<Integer> usersPostsIds = questions
                .map(Post::getId)
                .collect(Collectors.toList());

        return postLinkRepository.countByPostIdInAndLinkTypeId(usersPostsIds, LinkType.DUPLICATED);
    }

    @Override
    public int getNumberOfDiscussionsParticipated(Integer expertId, List<String> tags) {
        HashSet<Integer> ids = getAllDiscussionsIds(expertId);

        if (tags.size() > 0) {
            List<Post> posts = postRepository.findAllByIdIn(ids);
            return (int) posts.stream().filter(post -> post.containsTags(tags)).count();
        }
        return ids.size();
    }

    @Override
    public int getNumberOfComments(Integer expertId, List<String> tags) {
        if (tags.size() > 0) {
            List<Integer> ids = commentRepository.findPostIdsByUserId(expertId);
            List<Post> posts = postRepository.findAllByIdIn(ids);
            Set<Integer> verifiedQuestionsIds = posts.stream().filter(post -> post.containsTags(tags)).map(Post::getId).collect(Collectors.toSet());
            return (int) ids.stream()
                    .filter(verifiedQuestionsIds::contains)
                    .count();
        }
        return commentRepository.countByUserId(expertId);
    }

    @Override
    public Date getProfileCreationDate(Integer expertId) {
        return userRepository.findOne(expertId).getCreationDate();
    }

    @Override
    public int getNumberOfExpertReached(Integer expertId, List<String> tags) {
        prepareUser(expertId);

        HashSet<Integer> allDiscussionsIds = getAllDiscussionsIds(expertId);
        Set<Integer> allPostsInDiscussions = postRepository.findIdsByParentIdIn(allDiscussionsIds);
        allPostsInDiscussions.addAll(allDiscussionsIds);

        if (tags.size() > 0) {
            List<Post> posts = postRepository.findAllByIdIn(allDiscussionsIds);
            allPostsInDiscussions = posts.stream().filter(post -> post.containsTags(tags)).map(Post::getId).collect(Collectors.toSet());
        }

        if (allPostsInDiscussions.size() == 0) {
            return 0;
        }
        Set<Integer> allCommentAuthorsForPosts = commentRepository.findUserIdsByPostIdIn(allPostsInDiscussions);
        Set<Integer> allPostAuthorsForPosts = postRepository.findUserIdsByIdIn(allPostsInDiscussions);

        allPostAuthorsForPosts.addAll(allCommentAuthorsForPosts);
        allPostAuthorsForPosts.remove(expertId);
        return allPostAuthorsForPosts.size();
    }

    private HashSet<Integer> getAllDiscussionsIds(int expertId) {
        Stream<Integer> questionIds = usersPosts.getPosts(expertId).stream()
                .filter(p -> p.getPostTypeId() == PostType.QUESTION)
                .map(Post::getId);

        Stream<Integer> answerIds = usersPosts.getPosts(expertId).stream()
                .filter(p -> p.getPostTypeId() == PostType.ANSWER)
                .map(Post::getParentId);

        return Stream.concat(questionIds, answerIds).collect(Collectors.toCollection(HashSet::new));

    }
}
