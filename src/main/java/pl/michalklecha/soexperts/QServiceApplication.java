package pl.michalklecha.soexperts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(QServiceApplication.class, args);
    }
}
