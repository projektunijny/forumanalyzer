package pl.michalklecha.soexperts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.michalklecha.soexperts.services.Analyzer;

@Controller
public class ViewController {
    private Analyzer analyzer;

    @Autowired
    public ViewController(Analyzer analyzer) {
        this.analyzer = analyzer;
    }

    @RequestMapping(path = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(path = "/expert/{expertId}/")
    public String expert(Model model, @PathVariable("expertId") Integer expertId) {
        analyzer.checkIfUserExists(expertId);
        return "expert";
    }

}
