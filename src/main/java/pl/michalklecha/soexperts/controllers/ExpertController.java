package pl.michalklecha.soexperts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.michalklecha.soexperts.model.Response;
import pl.michalklecha.soexperts.services.Analyzer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ExpertController {

    private Analyzer analyzer;

    @Autowired
    public ExpertController(Analyzer analyzer) {
        this.analyzer = analyzer;
    }

    private void prepareUser(Integer expertId) {
        analyzer.checkIfUserExists(expertId);
        analyzer.prepareUser(expertId);
    }


    @RequestMapping(path = "/expert/{expertId}/questionPosted")
    public Response questionPosted(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Questions asked", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfQuestions(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/answersGiven")
    public Response answersGiven(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Answers given", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfAnswers(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/acceptedAnswers")
    public Response acceptedAnswers(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Accepted answers", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfAcceptedAnswers(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/duplicatedQuestions")
    public Response duplicatedQuestions(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Duplicated questions", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfDuplicatedQuestions(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/discussionsParticipated")
    public Response discussionsParticipated(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Discussions participated", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfDiscussionsParticipated(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/commentsGiven")
    public Response commentsGiven(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Comments given", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfComments(expertId, tags));
    }

    @RequestMapping(path = "/expert/{expertId}/expertReached")
    public Response expertReached(@PathVariable("expertId") Integer expertId, @RequestParam(required = false) String tag) {
        prepareUser(expertId);

        List<String> names = getNames("Experts Reached", tag);
        List<String> tags = getTags(tag);

        return Response.buildResponse(names, analyzer.getNumberOfExpertReached(expertId, tags));

    }

    @RequestMapping(path = "/expert/{expertId}/profileCreation")
    public Response profileCreation(@PathVariable("expertId") Integer expertId) {
        List<String> names = getNames("Creation of profile", null);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return Response.buildResponse(names, sdf.format(analyzer.getProfileCreationDate(expertId)));
    }

    private List<String> getTags(String tags) {
        if (tags == null) {
            return new ArrayList<>();
        }

        List<String> result = Arrays.asList(tags.split(","));
        result.replaceAll(String::trim);
        result.replaceAll(String::toLowerCase);
        List<String> resultTags = result.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        if (resultTags.isEmpty()) {
            throw new IllegalArgumentException("Empty tag given");
        }
        return resultTags;
    }

    private List<String> getNames(String name, String tag) {
        ArrayList<String> names = new ArrayList<>(Collections.singletonList(name));
        if (tag != null) {
            names.add(tag);
        }
        return names;
    }
}
