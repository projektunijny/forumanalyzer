package pl.michalklecha.soexperts.model;

import java.util.List;

public class Response {
    private String name;
    private Object value;

    private Response(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public static Response buildResponse(List<String> names, Object value) {
        if (names.size() > 1) {
            return new Response(names.get(0), buildResponse(names.subList(1, names.size()), value));
        }
        return new Response(names.get(0), value);
    }

    public static Response buildResponse(String name, Object value) {
        return new Response(name, value);
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
