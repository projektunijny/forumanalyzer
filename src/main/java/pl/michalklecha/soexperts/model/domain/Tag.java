package pl.michalklecha.soexperts.model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag {

    private static final char BEFORE = '<';
    private static final char AFTER = '>';

    @Id
    public int id;

    public String tagName;

    public int count;

    public int excerptPostId;

    public int wikiPostId;

    public int getId() {
        return id;
    }

    public static String prepareTag(String tag) {
        return BEFORE + tag + AFTER;
    }
}

