package pl.michalklecha.soexperts.model.domain.types;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class LinkType {

    public static final int LINKED = 1;
    public static final int DUPLICATED = 3;

    @Id
    private int id;

    private String name;

    public LinkType() {
    }

    public LinkType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static List<LinkType> getValues() {
        List<LinkType> result = new ArrayList<>();
        result.add(new LinkType(LINKED, "Linked"));
        result.add(new LinkType(DUPLICATED, "Duplicated"));
        return result;
    }
}
