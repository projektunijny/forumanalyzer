package pl.michalklecha.soexperts.model.domain.types;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PostType {

    public static final int QUESTION = 1;
    public static final int ANSWER = 2;

    @Id
    private int id;

    private String name;

    public PostType() {
    }

    public PostType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static List<PostType> getValues() {
        List<PostType> result = new ArrayList<>();
        result.add(new PostType(QUESTION, "Question"));
        result.add(new PostType(ANSWER, "Answer"));
        result.add(new PostType(3, "Wiki"));
        result.add(new PostType(4, "TagWikiExcerpt"));
        result.add(new PostType(5, "TagWiki"));
        result.add(new PostType(6, "ModeratorNomination"));
        result.add(new PostType(7, "WikiPlaceholder"));
        result.add(new PostType(8, "PrivilegeWiki"));
        return result;
    }

}
