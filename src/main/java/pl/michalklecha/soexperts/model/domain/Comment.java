package pl.michalklecha.soexperts.model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    public int id;

    public int postId;

    public int userId;

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }
}

