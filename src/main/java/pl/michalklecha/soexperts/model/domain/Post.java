package pl.michalklecha.soexperts.model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "post")
public class Post {

    @Id
    private Integer id;

    private Integer ownerUserId;

    private Integer parentId;

    private Integer postTypeId;

    private Integer acceptedAnswerId;

    private String tags;

    public int getId() {
        return id;
    }

    public Integer getOwnerUserId() {
        return ownerUserId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public Integer getPostTypeId() {
        return postTypeId;
    }

    public boolean containsTag(String tag) {
        return this.tags.contains(Tag.prepareTag(tag));
    }

    public boolean containsTags(List<String> tags) {
        for (String tag : tags) {
            if (this.tags == null || !this.tags.contains(Tag.prepareTag(tag))) {
                return false;
            }
        }
        return true;
    }
}