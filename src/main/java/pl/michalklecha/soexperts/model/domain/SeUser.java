package pl.michalklecha.soexperts.model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "se_user")
public class SeUser {

    public int reputation;
    private Date creationDate;
    public String displayName;
    public Date lastAccessDate;
    public String location;
    public int views;
    public int upVotes;
    public int downVotes;
    @Id
    private int id;

    public int getId() {
        return id;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
