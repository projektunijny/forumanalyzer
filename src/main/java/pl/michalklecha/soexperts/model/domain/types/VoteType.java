package pl.michalklecha.soexperts.model.domain.types;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class VoteType {

    @Id
    private int id;

    private String name;

    public VoteType() {
    }

    public VoteType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static List<VoteType> getValues() {
        List<VoteType> result = new ArrayList<>();
        result.add(new VoteType(1, "AcceptedByOriginator"));
        result.add(new VoteType(2, "UpMod"));
        result.add(new VoteType(3, "DownMod"));
        result.add(new VoteType(4, "Offensive"));
        result.add(new VoteType(5, "Favorite"));
        result.add(new VoteType(6, "Close"));
        result.add(new VoteType(7, "Reopen"));
        result.add(new VoteType(8, "BountyStart"));
        result.add(new VoteType(9, "BountyClose"));
        result.add(new VoteType(10, "Deletion"));
        result.add(new VoteType(11, "Undeletion"));
        result.add(new VoteType(12, "Spam"));
        result.add(new VoteType(15, "ModeratorReview"));
        result.add(new VoteType(16, "ApproveEditSuggestion"));

        return result;
    }

}
