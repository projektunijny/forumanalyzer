package pl.michalklecha.soexperts.model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "post_link")
public class PostLink {

    @Id
    public int id;

    public int postId;

    public int relatedPostId;

    public int linkTypeId;

    public Date creationDate;

    public int getId() {
        return id;
    }

    public int getLinkTypeId() {
        return linkTypeId;
    }
}
