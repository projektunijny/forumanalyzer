package pl.michalklecha.soexperts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.michalklecha.soexperts.model.domain.SeUser;

public interface UserRepository extends JpaRepository<SeUser, Integer> {
}
