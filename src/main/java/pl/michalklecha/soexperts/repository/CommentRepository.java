package pl.michalklecha.soexperts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.michalklecha.soexperts.model.domain.Comment;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Integer countByUserId(Integer expertId);

    @Query("SELECT c.postId FROM Comment c WHERE c.userId = :expertId")
    List<Integer> findPostIdsByUserId(@Param("expertId") Integer expertId);

    @Query("select c.userId FROM Comment c WHERE c.postId IN (:postIds)")
    Set<Integer> findUserIdsByPostIdIn(@Param("postIds") Collection<Integer> postIds);
}
