package pl.michalklecha.soexperts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.michalklecha.soexperts.model.domain.PostLink;

import java.util.List;

public interface PostLinkRepository extends JpaRepository<PostLink, Integer> {
    Integer countByPostIdInAndLinkTypeId(List<Integer> ids, Integer linkTypeId);
}
