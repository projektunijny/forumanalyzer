package pl.michalklecha.soexperts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.michalklecha.soexperts.model.domain.Post;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findAllByOwnerUserId(Integer expertId);

    List<Post> findAllByIdIn(Collection<Integer> ids);

    List<Post> findAllByAcceptedAnswerIdIn(Collection<Integer> answers);

    @Query("SELECT p.id FROM Post p WHERE p.parentId IN (:ids)")
    Set<Integer> findIdsByParentIdIn(@Param("ids") Collection<Integer> ids);

    Integer countByAcceptedAnswerIdIn(Collection<Integer> answers);

    @Query("SELECT p.ownerUserId FROM Post p WHERE p.id IN (:ids)")
    Set<Integer> findUserIdsByIdIn(@Param("ids") Collection<Integer> allDiscussionsIds);

}
