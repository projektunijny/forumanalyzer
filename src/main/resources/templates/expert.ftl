<#--noinspection ALL-->
<html>
<body>
<a href="questionPosted">
    <button>Ilość pytań</button>
</a>
<form action="questionPosted" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość pytań (tag)</button>
</form>
<#---------------->
<a href="answersGiven">
    <button>Ilość odpowiedzi</button>
</a>
<form action="answersGiven" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość odpowiedzi (tag)</button>
</form>
<#---------------->
<a href="acceptedAnswers">
    <button>Ilość zaakceptowanych odpowiedzi</button>
</a>
<form action="acceptedAnswers" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość zaakceptowanych odpowiedzi (tag)</button>
</form>
<#---------------->
<a href="duplicatedQuestions">
    <button>Ilość zduplikowanych pytań</button>
</a>
<form action="duplicatedQuestions" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość zduplikowanych pytań (tag)</button>
</form>
<#---------------->
<a href="discussionsParticipated">
    <button>Ilość dyskusji w ogóle</button>
</a>
<form action="discussionsParticipated" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość dyskusji w ogóle (tag)</button>
</form>
<#---------------->
<a href="commentsGiven">
    <button>Ilość komentarzy</button>
</a>
<form action="commentsGiven" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość komentarzy (tag)</button>
</form>
<#---------------->
<a href="expertReached">
    <button>Ilość ekspertów z którymi miał kontakt</button>
</a>
<form action="expertReached" method="get">
    Tag: <input name="tag">
    <button type="submit">Ilość ekspertów z którymi miał kontakt (tag)</button>
</form>
<#---------------->
<a href="profileCreation">
    <button>Od kiedy aktywny</button>
</a>
</body>
</html>