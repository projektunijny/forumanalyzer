<#--noinspection ALL-->
<html>
<body>
<div>
    ID experta:
</div>
<form id="form" action="/expert/" method="get" onsubmit="mySubmit();">
    <input type="text" id="expertId"/>
    <input type="submit"/>
    <script>
        function mySubmit() {
            let expertId = document.getElementById('expertId').value;
            document.getElementById('form').action = '/expert/' + expertId + '/';
        }
    </script>
</form>
</body>
</html>